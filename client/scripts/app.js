"use srict";
var angular = require('angular'),
  log = require('loglevel'),
  Events = require('./Events');
log.setDefaultLevel('debug');
require('angular-ui-router');
require('angular-ui-bootstrap');
var _ = require('lodash');
var app = angular.module('FantasyNBA', [
  'ui.router',
  'ui.bootstrap'
]);

module.exports = app;

var routes = require('./routes');
require('./Services/ResponseInterceptor');

function toStateProperties(routeConfig) {
  var stateProperties = {
    url: routeConfig.url,
    templateUrl: routeConfig.templateUrl
  };
  if (routeConfig.controller) {
    stateProperties.controller = routeConfig.controller;
  }

  if (routeConfig.views) {
    stateProperties.views = {};
    _.each(routeConfig.views, function(view){
      var viewName = view.name;
      if(viewName) {
        stateProperties.views[viewName] = {
          templateUrl : view.templateUrl
        };
      }
    });
  }
  return stateProperties;
}

app.config(
  function ($httpProvider, $stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise('/login');

    _.each(routes, function (routeConfig) {
      log.debug("app started");
      $stateProvider.state(routeConfig.stateName,
        toStateProperties(routeConfig));
    });
  });

app.config(
  function ($httpProvider) {
    $httpProvider.interceptors.push('ResponseInterceptor');
  });

app.run(function ($rootScope, $state) {
  log.debug("start app");
  $rootScope.$state = $state;
  log.debug("start app");
  $rootScope.updateBalance = function() {
    $rootScope.$broadcast(Events.BALANCE_UPDATED);
  };
});


'use strict';

var app = require('../app');

app.directive('playerlist', function () {
  return {
    replace: true,
    scope: true,
    templateUrl: '../templates/segments/playerlist.html',
    link: function (scope, elem, attrs) {
      if (attrs && attrs.position) {
        scope.positionFilter = attrs.position;
      }
    }
  };
});

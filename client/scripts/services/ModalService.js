'use strict';

var app = require('../app');

app.factory('ModalService', function ($uibModal) {
  return {
    open : function(templateUrl, controllerName, data) {
      var modalProperties = {
        animation: true,
        templateUrl: templateUrl,
        controller: controllerName
      };

      if(data){
        modalProperties.resolve = {
          data: function () {
            return data;
          }
        };
      }

      return $uibModal.open(modalProperties);
    }
  };
});

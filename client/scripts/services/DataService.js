'use strict';

var app = require('../app'),
  DataServiceHelper = require('../utils/DataServiceHelper'),
  AppConfig = require('../AppConfig');
require('./SessionService');

app.factory('DataService', function ($http, SessionService, $q) {
  var dataServiceHelper = new DataServiceHelper($http, AppConfig.WEB_SERVICE_URL , $q, SessionService);

  return {
    login: function (username, password) {
      return dataServiceHelper.doPost('/login', {}, {
        username: username,
        password: password
      });
    },
    logout: function () {
      return dataServiceHelper.doPost('/secure/logout', {}, {});
    },
    getCreditCardPaymentSecurityInfo: function () {
      return dataServiceHelper.doGet('/secure/payment/securityinfo', {});
    },
    getBalance: function () {
      return dataServiceHelper.doGet('/secure/balance', {});
    },
    getAllPlayers: function () {
      return dataServiceHelper.doGet('/secure/players/all', {});
    },
    submitRoster: function (rosterSubmission) {
      return dataServiceHelper.doPost('/secure/submitroster', {}, rosterSubmission);
    }
  };
});

'use strict';
var app = require('../app');
require('./SessionService');

app.factory('ResponseInterceptor', function ($q,$injector,SessionService) {
  return {
    responseError: function (response) {
      // Session has expired
      if (response.status === 401) {
        SessionService.deleteCurrentSession();
        $injector.get('$state').go('login');
      }

      return $q.reject(response);
    }
  };
});

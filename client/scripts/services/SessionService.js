'use strict';

var app = require('../app');

app.factory('SessionService', function ($rootScope) {

  var currentSession;

  return {
    getCurrentSessionId: function () {
      if (currentSession) {
        return currentSession.sessionId;
      }
      return undefined;
    },
    getCurrentSession: function () {
      return currentSession;
    },
    createSession: function (sessionObj) {
      currentSession = sessionObj;
      currentSession.balance = 0.0;
      $rootScope.isLoggedIn = true;
    },
    deleteCurrentSession: function () {
      currentSession = undefined;
      $rootScope.isLoggedIn = false;
    },
    isLoggedIn : function() {
      return $rootScope.isLoggedIn;
    },
    updateBalance: function (newBalance) {
      if (currentSession) {
        currentSession.balance = newBalance;
      }
    },
    getBalance: function () {
      if (currentSession) {
        return currentSession.balance;
      }
      return 0;
    }
  };
});

'use strict';

var app = require('../app'),
  AppConfig = require('../AppConfig');
var Payfirma = require('payfirma');

require('./DataService');
require('./SessionService');


app.factory('CreditCardService', function ($q, DataService, SessionService) {
  var key;


  function createPurchaseHandler(amount, creditCardInfo, userId, deferred) {
    new Payfirma(key, {
        'card_number': creditCardInfo.cardNumber,
        'card_expiry_month': creditCardInfo.expiryMonth,
        'card_expiry_year': creditCardInfo.expiryYear,
        'cvv2': creditCardInfo.cvv
      }, {
        'userid': userId,
        'securitytoken': userId,
        'amount': amount
      },
      AppConfig.WEB_SERVICE_URL + '/payment/purchase', function (result) {
        deferred.resolve(JSON.parse(result));
      });
  }

  return {
    purchase: function (amount, creditCardInfo) {
      var session = SessionService.getCurrentSession();
      var deferred = $q.defer();
      if (session) {
        var userId = session.userData.userId;
        if (!key) {
          DataService.getCreditCardPaymentSecurityInfo().then(function (securityInfo) {
            if (securityInfo && securityInfo.publicKey) {
              key = securityInfo.publicKey;
              createPurchaseHandler(amount, creditCardInfo, userId, deferred);
            }
          }, function (error) {
            deferred.reject(error);
          });
        } else {
          createPurchaseHandler(amount, creditCardInfo, userId, deferred);
        }
      } else {
        deferred.reject({
          error: "User is not logged in"
        });
      }
      return deferred.promise;
    },
    getAllAvailableYearsDropDownOptions: function () {
      var startYear = new Date().getFullYear();
      var years = [];
      for (var i = 0; i < 10; i++) {
        var year = (startYear + i) + "";
        years.push({
          value: year.slice(-2),
          label: year
        });
      }
      return years;
    },
    getAllAvailableMonthsDropDownOptions: function () {
      var months = [];
      for (var i = 1; i <= 12; i++) {
        var monthString = i < 10 ? "0" + i : i + "";
        months.push({
          value: monthString,
          label: monthString
        });
      }
      return months;
    }
  };
});

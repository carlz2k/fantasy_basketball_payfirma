'use strict';

var _ = require('lodash');


module.exports = function ($http, serviceUrl, $q, SessionService) {

  var self = this;
  var RequestMethod = {
    GET: 'GET',
    POST: 'POST',
    PUT: 'PUT',
    DELETE: 'DELETE'
  };

// if params are part of the url we replace them with values
// and remove them from parameter list
  function parseUrlWithParams(url, params) {

    if (!params) {
      return url;
    }

    var keys = _.keys(params);

    _.each(keys, function (key) {
      var param = ":" + key;
      if (url.indexOf(param) !== -1) {
        url = url.replace(param, params[key]);
      }
    });

    return url;
  }

  function getResourceByRestUtil(url, params, method, data) {

    if (!method) {
      method = RequestMethod.GET;
    }

    var newUrl = parseUrlWithParams(serviceUrl + url, params);

    //we use our own deferred to unwrap the requested data from the returned data object
    return httpCall($http, method, data, newUrl);
  }

  function httpCall($http, method, data, serviceUrl) {
    var headerValues = {'Content-Type': 'application/json', 'Accept': 'application/json'};
    var sessionId = SessionService.getCurrentSessionId();
    if(sessionId) {
      headerValues.sessionid = sessionId;
      //because node forces all header names into lowercases, we have to use lower case for this header name:(
    }
    var headers = {headers: headerValues};
    if (method === RequestMethod.POST) {
      return processPromise($http.post(serviceUrl, data, headers));
    } else {
      return processPromise($http.get(serviceUrl, headers));
    }
  }

  function processPromise(promise){
    var deferred = $q.defer();
    promise.then(
      function(response){
        if(response && response.status===200) {
          deferred.resolve(response.data);
        } else {
          deferred.reject(response.data);
        }
      },
      function(error){
        deferred.reject(error.data);
      }
    );
    return deferred.promise;
  }

  self.doGet = function (url, params) {
    return getResourceByRestUtil(url, params, RequestMethod.GET);
  };

  self.doPost = function (url, params, data) {
    return getResourceByRestUtil(url, params, RequestMethod.POST, data);
  };
};



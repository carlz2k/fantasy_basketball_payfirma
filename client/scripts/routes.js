'use strict';

require('./controllers/LoginController');
require('./controllers/DepositController');
require('./controllers/GameController');
require('./controllers/PickRosterController');
require('./controllers/GameResultController');
require('./controllers/MainController');

var routes = [
  {
    stateName: 'login',
    url:'/login',
    templateUrl:'../templates/login.html',
    controller:'LoginController'
  },
  {
    stateName: 'game',
    url:'/game',
    templateUrl:'../templates/game.html',
    controller:'GameController'
  },
  {
    stateName: 'game.pickRoster',
    url:'/pickroster',
    templateUrl:'../templates/game.pickroster.html',
    controller:'PickRosterController'
  },
  {
    stateName: 'game.winner',
    url:'/winner/:roster/:winAmount',
    templateUrl:'../templates/game.winner.html',
    controller:'GameResultController'
  },
  {
    stateName: 'game.loser',
    url:'/loser',
    templateUrl:'../templates/game.loser.html'
  },
  {
    stateName: 'deposit',
    url:'/deposit',
    templateUrl:'../templates/deposit.html',
    controller:'DepositController'
  }
];

module.exports = routes;

'use strict';

var app = require('../app');

app.controller('DepositConfirmationController', function ($scope, $state, $modalInstance) {
  $scope.goToPickRoster = function() {
    $modalInstance.close();
    $state.go('game.pickRoster');
  };
});

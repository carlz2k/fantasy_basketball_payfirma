'use strict';

var app = require('../app');
var _ = require('lodash');
require('../services/DataService');
require('../services/ModalService');
require('../directives/PlayerList');
require('./InsufficientBalanceModalController');
require('./CommonModalController');

app.controller('PickRosterController', function ($scope, $rootScope, $state, DataService, SessionService, ModalService) {
  DataService.getAllPlayers()
    .then(function (response) {
      $scope.allPlayers = response;
    });


  $scope.submitRoster = function () {
    var cost = $scope.cost;
    var isRosterValid = validateRoster();
    if (cost && cost <= SessionService.getBalance() && isRosterValid) {
      var roster = createRoster();
      DataService.submitRoster({
        roster: roster,
        cost: cost
      }).then(function (result) {
        $rootScope.updateBalance();
        if (result && result.isWinner && result.winAmount) {
          $state.go('game.winner', {roster: roster, winAmount: result.winAmount});
        } else {
          $state.go('game.loser');
        }

      }, function () {
        ModalService.open('../templates/modals/insufficientbalancemodal.html', 'InsufficientBalanceModalController');
      });
    } else if (!isRosterValid) {
      ModalService.open('../templates/modals/pickrosterfailedmodal.html', 'CommonModalController');
    } else if (cost && cost > SessionService.getBalance()) {
      ModalService.open('../templates/modals/insufficientbalancemodal.html', 'InsufficientBalanceModalController');
    } else {
      ModalService.open('../templates/modals/pickrosterfailedmodal.html', 'CommonModalController');
    }
  };

  function createRoster() {
    var roster = [];
    _.each($scope.allPlayers, function (player) {
      if (player.selected) {
        roster.push(player.id);
      }
    });
    return roster;
  }

  function validateRoster() {
    var numberOfCenters = 0;
    var numberOfGuards = 0;
    var numberOfForwards = 0;

    _.each($scope.allPlayers, function (player) {
      if (player.selected) {
        if (player.position === 'CENTER') {
          numberOfCenters++;
        } else if (player.position === 'FORWARD') {
          numberOfForwards++;
        } else if (player.position === 'GUARD') {
          numberOfGuards++;
        }
      }
    });
    return numberOfCenters === 1 && numberOfForwards === 2 && numberOfGuards === 2;
  }
});

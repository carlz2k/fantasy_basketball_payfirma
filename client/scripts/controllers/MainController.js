'use strict';

var app = require('../app');
var Events = require('../Events');

require('../services/SessionService');
require('../services/DataService');

app.controller('MainController', function ($scope, $state, DataService, SessionService) {
  $scope.getName = function () {
    var currentSession = SessionService.getCurrentSession();
    if (currentSession) {
      return currentSession.userData.firstName + " " + currentSession.userData.lastName;
    }
    return undefined; //for readability;
  };

  $scope.isLoggedIn = function () {
    var isloggedin = SessionService.isLoggedIn() ?
      SessionService.isLoggedIn() : false;
    return isloggedin;
  };

  $scope.getBalance = function () {
    return SessionService.getBalance();
  };

  $scope.logout = function () {
    DataService.logout()
      .then(function () {
        SessionService.deleteCurrentSession();
        $state.go('login');
      });
  };

  $scope.$on(Events.BALANCE_UPDATED, function () {
    DataService.getBalance()
      .then(function (balance) {
        SessionService.updateBalance(balance);
      });
  });
});

'use strict';

var app = require('../app');

app.controller('InsufficientBalanceModalController', function ($scope, $state, $modalInstance) {
  $scope.goToDeposit = function() {
    $modalInstance.close();
    $state.go('deposit');
  };
});

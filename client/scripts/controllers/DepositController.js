'use strict';

var app = require('../app');

require('../services/CreditCardService');
require('../services/ModalService');
require('./DepositConfirmationController');
require('./CommonModalController');

app.controller('DepositController', function ($scope, $rootScope, CreditCardService, ModalService) {
  var submitInProgress = false;

  $scope.years = CreditCardService.getAllAvailableYearsDropDownOptions();

  $scope.months = CreditCardService.getAllAvailableMonthsDropDownOptions();

  $scope.deposit = function() {
    submitInProgress = true;
    CreditCardService.purchase($scope.amount, {
      cardNumber : $scope.cardNumber,
      expiryYear : $scope.expiryYear,
      expiryMonth : $scope.expiryMonth,
      cvv : $scope.cvv
    }).then(function(result){
      submitInProgress = false;
      if(result && result.success) {
        $rootScope.updateBalance();
        openSuccessfulDepositConfirmation();
      } else {
        openRejectedDepositConfirmation();
      }

    }, function(){
      submitInProgress = false;
      openRejectedDepositConfirmation();
    });
  };

  function openSuccessfulDepositConfirmation() {
    ModalService.open('../templates/modals/depositconfirmationmodal.html', 'DepositConfirmationController');
  }

  function openRejectedDepositConfirmation() {
    ModalService.open('../templates/modals/depositfailedmodal.html', 'CommonModalController');
  }

  $scope.isSubmitButtonDisabled = function() {
    return $scope.creditCardForm.$invalid || submitInProgress;
  };
});

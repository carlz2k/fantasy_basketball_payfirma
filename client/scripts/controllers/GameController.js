'use strict';

var app = require('../app');
require('../services/DataService');
require('../services/ModalService');
require('../directives/PlayerList');
require('./InsufficientBalanceModalController');
require('./CommonModalController');

app.controller('GameController', function ($scope, DataService) {
  DataService.getAllPlayers()
    .then(function (response) {
      $scope.allPlayers = response;
    });
});

'use strict';

var app = require('../app');
require('../services/DataService');
require('../services/ModalService');
require('../services/SessionService');

app.controller('LoginController', function ($scope, $rootScope, $state, DataService, SessionService, ModalService) {
  $scope.login = function () {
    DataService.login($scope.username, $scope.password)
      .then(function (response) {
        if(response.success) {
          SessionService.createSession(response.session);
          $rootScope.updateBalance();
          $state.go('game.pickRoster');
        } else {
          ModalService.open('../templates/modals/unsuccessfulloginmodal.html', 'CommonModalController');
        }

      }, function () {
        ModalService.open('../templates/modals/unsuccessfulloginmodal.html', 'CommonModalController');
      });
  }
});

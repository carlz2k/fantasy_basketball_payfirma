'use strict';

var app = require('../app');
var _ = require('lodash');

app.controller('GameResultController', function ($scope, $stateParams) {
  var playerIdsOfRoster = $stateParams.roster.split(",");
  $scope.selectedPlayers = _.map(playerIdsOfRoster, function (playerId) {
    return _.find($scope.allPlayers, function (player) {
      return player.id === parseInt(playerId);
    });
  });

  $scope.winAmount = $stateParams.winAmount;
});

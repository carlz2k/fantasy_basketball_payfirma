'use strict';

var app = require('../app');

app.controller('CommonModalController', function ($scope,$modalInstance) {
  $scope.close = function() {
    $modalInstance.close();
  };
});

'use strict';
var httpclient = require('../../utils/httpclient');
var _ = require('lodash');
var AppConfig = require('../../AppConfig');
//TODO: do not hardcode, find a simple configuration lookup service


function addRequiredPayfrimaQueryParameters(originalQueryParams) {
  return _.assign({
    key: AppConfig.PAYFIRMA.API_KEY,
    merchant_id: AppConfig.PAYFIRMA.MERCHANT_ID,
    test_mode: AppConfig.PAYFIRMA.TEST_MODE
  }, originalQueryParams);
}
var api = {

  createCustomer: function (customerInfo) {
    return httpclient.formPost(AppConfig.PAYFIRMA.VAULT_URL + customerInfo.email,
      addRequiredPayfrimaQueryParameters(), {
        card_number: customerInfo.cardNumber,
        card_expiry_month: customerInfo.expiryMonth,
        card_expiry_year: customerInfo.expiryYear,
        cvv2: customerInfo.cvv,
        first_name: customerInfo.firstName,
        last_name: customerInfo.lastName
      }).then(function (response) {
        console.log("response" + response);
        return response;
      }).catch(function (error) {
        return error;
      });

  },

  purchaseWithCreditCardInfoToken: function (amount, creditCardInfoToken) {
    return httpclient.formPost(AppConfig.PAYFIRMA.SALE_URL,
      addRequiredPayfrimaQueryParameters({
        amount: amount
      }), {
        token : creditCardInfoToken
      }).then(function (response) {
        if (response) {
          return JSON.parse(response.body);
        }
      }).catch(function (error) {
        return error;
      });
  },

  purchase: function (amount, creditCardInfo) {
    return httpclient.formPost(AppConfig.PAYFIRMA.SALE_URL,
      addRequiredPayfrimaQueryParameters({
        amount: amount
      }), {
        card_number: creditCardInfo.cardNumber,
        card_expiry_month: creditCardInfo.expiryMonth,
        card_expiry_year: creditCardInfo.expiryYear,
        cvv2: creditCardInfo.cvv
      }).then(function (response) {
        if (response) {
          return JSON.parse(response.body);
        }
      }).catch(function (error) {
        return error;
      });
  },

  getSecurityInfo: function () {
    return {
      publicKey : AppConfig.PAYFIRMA.PUBLIC_KEY
    };
  },

  isSecurityTokenValid : function(securityToken, userId) {
    return securityToken && securityToken === userId;
  }
};

module.exports = api;


'use strict';
var _ = require('lodash');
var sessionMap = {};

function removeSessionBySessionId(sessionId) {
  if (sessionMap[sessionId]) {
    delete sessionMap[sessionId];
  }
}

function getSessionByUserId(userId) {
  return _.find(sessionMap, function (session) {
    return session.userData.userId === userId;
  });
}

function removeSessionByUser(userData) {
  var existingSessionOfUser = getSessionByUserId(userData.userId);
  if (existingSessionOfUser) {
    removeSessionBySessionId(existingSessionOfUser.sessionId);
  }
}

function generateSessionId() {
  return Math.floor((Math.random() * 10000) + 1) + "";
}

function createSessionId() {
  var sessionId = generateSessionId();
  while (sessionMap.sessionId) {
    sessionId = generateSessionId();
  }
  return sessionId;
}

function createSessionObj(sessionId, userData) {
  var sessionObj = {
    sessionId: sessionId,
    userData: userData
  };

  if (userData.balance) {
    sessionObj.balance = userData.balance;
  } else {
    sessionObj.balance = 0.0;
  }

  return sessionObj;
}

module.exports = {
  createSession: function (userData) {
    removeSessionByUser(userData);
    var sessionId = createSessionId();
    var sessionObj = createSessionObj(sessionId, userData);
    sessionMap[sessionId] = sessionObj;
    return sessionObj;
  },
  getSession: function (sessionId) {
    if (sessionId) {
      return sessionMap[sessionId];
    }
    return undefined;
  },
  getSessionByUserId: function (userId) {
    return getSessionByUserId(userId);
  },
  removeSession: function (sessionId) {
    removeSessionBySessionId(sessionId);
  },
  getBalance: function (sessionId) {
    var session = this.getSession(sessionId);
    if (session) {
      return session.balance;
    }
    return undefined;
  },
  getUserId: function (sessionId) {
    var session = this.getSession(sessionId);
    if (session) {
      return session.userData.userId;
    }
    return undefined;
  },
  isLoggedIn: function (sessionId) {
    return sessionId && this.getSession(sessionId);
  }
};

'use strict';
var _ = require('lodash');
var users = [
  {
    firstName: 'Carl',
    lastName: 'Zhang',
    email: 'carl@email.com',
    password: 'password',
    userId: "3"
  },
  {
    firstName: 'money',
    lastName: 'user',
    email: 'money@email.com',
    password: 'password',
    userId: "4",
    balance: 40
  },
  {
    firstName: 'Tester',
    lastName: 'Tester',
    email: 'tester@email.com',
    password: 'password',
    userId: "2"
  }
];
module.exports = {
  authenticate: function (username, passowrd) {
    return _.find(users, function (user) {
      return user.email === username && user.password === passowrd;
    });
  }
};

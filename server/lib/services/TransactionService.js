'use strict';
var SessionService = require('./SessionService');

module.exports = {
  //because we do not have a place to user's balance
  //we will store it in SessionService for simplicity
  //the balance will be lost after a server restart or log out
  addToBalance: function (amount, userId) {
    var session = SessionService.getSessionByUserId(userId);
    if (session) {
      //becasue NodeJs is single-threaded
      //there is no need to do synchronization if get current balance, add balance, and update current balance are all done in one function
      //there will be racing conditions if they are done asynchronously
      var balance = session.balance;
      if (!balance) {
        balance = 0.0; //probably not a good thing to store currency using dobule, but in javascript there is no choice except using thirdparty libraries
        //use double for simplicity
      }
      balance = balance + amount;
      if (balance >= 0.0) {
        session.balance = Math.round((balance + 0.00001) * 100) / 100;
        return true;
      } else {
        return false;
      }
    }
  }
};

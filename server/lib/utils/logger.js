'use strict';
var bunyan = require('bunyan');
var log = bunyan.createLogger({
  name: 'StockTwits',
  src: true,
  serializers: bunyan.stdSerializers
});

module.exports = log;

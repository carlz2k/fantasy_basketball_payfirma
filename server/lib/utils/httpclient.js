'use strict';
var request = require('request');
var promise = require('promise');
var queryString = require('querystring');
var _ = require('lodash');

function isValidStatusCode(statusCode) {
  return _.includes([200,201,302],statusCode);
}
function getResponseHandler(resolve, reject) {
  return function (err, httpResponse, body) {
    if (err ||
      (httpResponse && !isValidStatusCode(httpResponse.statusCode))) {
      reject({
        error: err,
        httpResponse: httpResponse
      });
    } else {
      resolve({
        httpResponse: httpResponse,
        body: body
      });
    }
  };
}

function createOptions(extraOptions) {
  var TIMEOUT = 20*1000;
  return _.extend(extraOptions, {timeout:TIMEOUT});
}

var httpclient = {
  formPost: function (url, queryParams, formData) {
    if(queryParams) {
      url = url+'?'+queryString.stringify(queryParams);
    }
    return new promise(function (resolve, reject) {
      request.post(createOptions({
          url: url,
          formData: formData
        }),
        getResponseHandler(resolve, reject)
      );
    });
  }
};
module.exports = httpclient;

'use strict';
var Hapi = require('hapi');
var _ = require('lodash');

var server = new Hapi.Server();
server.connection({port: 3000, routes: { cors: {
  origin: ['*'],
  headers: ['Authorization', 'Content-Type', 'If-None-Match', 'Accept', 'sessionId']
} }});

module.exports = {
  addRoutes: function (routes) {
    _.each(routes, function (rule) {
      server.route(rule);
    });
  },
  handleAuthorization: function(onRequestHandler) {
    server.ext({
      type: 'onRequest',
      method: function (request, reply) {
        return onRequestHandler(request, reply);
      }
    });
  },
  start: function () {
    server.start(
      function () {
        console.log('Server running at:', server.info.uri);
      }
    );
  }
};

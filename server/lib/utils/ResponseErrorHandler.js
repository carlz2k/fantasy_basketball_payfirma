"use strict";

var Boom = require('boom');

module.exports = {
  missingParameter : function(reply) {
    return reply(Boom.badRequest('missing parameters'));
  },
  error : function(reply, error) {
    return reply(Boom.badImplementation(error));
  },
  unauthorized : function(reply) {
    return reply(Boom.unauthorized('user not authorized'));
  }
};


'use strict';

var GameDataStore = require('../game/data/DataStore');
var _ = require('lodash');

function getWinningMultiplier() {
  return Math.random() * 3 + 1;
}

function isWinner(roster) {
  if (roster && roster.length) {
    var sumOfTotalPointsOfRoster = _.reduce(roster, function (sum, playerId) {
      var totalPoints = 0;
      var player = GameDataStore.getPlayer(playerId);
      if (player) {
        totalPoints = player.stats.totalPoints;
      }
      return sum + totalPoints;
    }, 0);
    var averageOfTotalPointsOfRoster = sumOfTotalPointsOfRoster / roster.length;
    return averageOfTotalPointsOfRoster > GameDataStore.getTotalPointsAverage();
  }
}

function calculateResult(rosterSubmission) {
  var result = false;
  var winAmount = 0;
  if (rosterSubmission) {
    result = isWinner(rosterSubmission.roster);
    if (result) {
      var winningMultiplier = getWinningMultiplier();
      winAmount = Math.round(rosterSubmission.cost * winningMultiplier *100)/100;
    }
  }

  return {
    isWinner: result,
    winAmount: winAmount
  };
}

module.exports = {
  getAllPlayers: function () {
    return GameDataStore.getAllPlayers();
  },
  submitRoster: function (rosterSubmission) {
    return calculateResult(rosterSubmission);
  }
};

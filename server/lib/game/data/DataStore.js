"use strict";
var _ = require('lodash');
var Position = require('./Position');
var Player = require('./Player');

var players = [
  {
    id: 1,
    firstName: 'Lebron',
    lastName: 'James',
    position: Position.FORWARD,
    stats: {
      totalPoints: 1743
    }
  },
  {
    id: 2,
    firstName: 'Blake',
    lastName: 'Griffin',
    position: Position.FORWARD,
    stats: {
      totalPoints: 1469
    }
  },
  {
    id: 3,
    firstName: 'Dirk',
    lastName: 'Nowitzki',
    position: Position.FORWARD,
    stats: {
      totalPoints: 1333
    }
  },
  {
    id: 4,
    firstName: 'Anthony',
    lastName: 'Davis',
    position: Position.FORWARD,
    stats: {
      totalPoints: 1656
    }
  },
  {
    id: 5,
    firstName: 'DeMarcus',
    lastName: 'Cousins',
    position: Position.CERNTER,
    stats: {
      totalPoints: 1421
    }
  },
  {
    id: 6,
    firstName: 'Marc',
    lastName: 'Gasol',
    position: Position.CERNTER,
    stats: {
      totalPoints: 1413
    }
  },
  {
    id: 7,
    firstName: 'Brook',
    lastName: 'Lopez',
    position: Position.CERNTER,
    stats: {
      totalPoints: 1236
    }
  },
  {
    id: 8,
    firstName: 'Stephen',
    lastName: 'Curry',
    position: Position.GUARD,
    stats: {
      totalPoints: 1900
    }
  },
  {
    id: 9,
    firstName: 'Russell',
    lastName: 'Westbrook',
    position: Position.GUARD,
    stats: {
      totalPoints: 1886
    }
  },
  {
    id: 10,
    firstName: 'Klay',
    lastName: 'Thompson',
    position: Position.GUARD,
    stats: {
      totalPoints: 1668
    }
  },
  {
    id: 11,
    firstName: 'Chris',
    lastName: 'Paul',
    position: Position.GUARD,
    stats: {
      totalPoints: 1564
    }
  }
];

module.exports = {
  getAllPlayers: function () {
    return _.map(players, function (player) {
      return new Player(player);
    });
  },
  getPlayer: function (id) {
    return _.find(players, function (player) {
      return player.id === id;
    });
  },
  getTotalPointsAverage: function () {
    if (players && players.length) {
      var sumOfTotalPoints = _.reduce(players, function (sum, player) {
        return sum + player.stats.totalPoints;
      }, 0);
      return sumOfTotalPoints / players.length;
    }
  }
};

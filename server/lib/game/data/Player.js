"use strict";
var Stats = require('./Stats');

module.exports = function(userProperties){
  var self = this;
  self.id = userProperties.id;
  self.firstName = userProperties.firstName;
  self.lastName = userProperties.lastName;
  self.position = userProperties.position;
  self.stats = new Stats(userProperties.stats);
};

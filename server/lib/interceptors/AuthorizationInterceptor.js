'use strict';

var SessionService = require('../services/SessionService');
var routes = require('../restapi/routes');
var ResponseErrorHandler = require('../utils/ResponseErrorHandler');

module.exports = {
  //I use a simple interceptor to handle pre-authorization just because i don't have enough time
  //TODO: probably write a custom hapi-auth strategy
  authorize: function(request, reply) {
    if(routes.requireAuthorization(request)) {
      var sessionId = request.headers.sessionid;
      if(!sessionId || !SessionService.isLoggedIn(sessionId)) {
        return ResponseErrorHandler.unauthorized(reply);
      }
    }

    return reply.continue();

  }
};

'use strict';

var dataProvider = require('./DataProvider');
var SECURE_URL_PREFIX = '/secure';
module.exports = {
  requireAuthorization: function (request) {
    var requestUrl = request.url.path;
    var method = request.method;
    return requestUrl.indexOf(SECURE_URL_PREFIX) !== -1 && (method === 'get' || method === 'post');
  },
  routes: [
    {
      method: 'GET',
      path: '/',
      handler: function (request, reply) {
        reply('payment api!');
      }
    }, {
      method: 'POST',
      path: '/payment/purchase',
      handler: dataProvider.purchase
      //painful because purchase method is called by payfirma,
      //we cannot pre-authorize this method
      //sessinid has to be part of the api call:(
    }, {
      method: 'GET',
      path: SECURE_URL_PREFIX + '/payment/securityinfo',
      handler: dataProvider.getCreditCardPaymentSecurityInfo
    }, {
      method: 'POST',
      path: '/login',
      handler: dataProvider.login
    },{
      method: 'POST',
      path: SECURE_URL_PREFIX + '/logout',
      handler: dataProvider.logout
    }, {
      method: 'GET',
      path: SECURE_URL_PREFIX + '/players/all',
      handler: dataProvider.getAllPlayers
    }, {
      method: 'GET',
      path: SECURE_URL_PREFIX + '/balance',
      handler: dataProvider.getBalance
    },{
      method: 'POST',
      path: SECURE_URL_PREFIX + '/submitroster',
      handler: dataProvider.submitRoster
    }
  ]
};


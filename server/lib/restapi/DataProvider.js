'use strict';
var log = require('loglevel');
var payfirmaService = require('../payment/payfirma/PayfirmaService');
var ErrorHandler = require('../utils/ResponseErrorHandler');
var SessionService = require('../services/SessionService');
var UserManager = require('../services/UserManager');
var GameEngine = require('../game/GameEngine');
var TransactionService = require('../services/TransactionService');

function toNumber(numberString) {
  if (numberString && !isNaN(numberString)) {
    return parseFloat(numberString);
  }
  return undefined;
}

function processCreditCardTransaction(creditcardInfoToken, amount, userId, reply) {
  return payfirmaService.purchaseWithCreditCardInfoToken(amount, creditcardInfoToken).then(function (response) {
    if (response && response.result === 'approved') {
      if (TransactionService.addToBalance(amount, userId)) {
        return reply({
          success: true,
          transactionId: response.transaction_id
        });
      }
    }

    return reply({
      success: false,
      error: "transaction has been rejected."
    });
  }).catch(function (error) {
    return ErrorHandler.error(reply, error);
  });
}

module.exports = {
  //TODO: probably need another level of abstraction
  //to abstract request and reply to make the code more testable
  login: function (request, reply) {
    log.debug("/login");
    if (request && request.payload) {
      var payload = request.payload;
      var username = payload.username;
      var password = payload.password;
      if (username && password) {
        var user = UserManager.authenticate(username, password);
        if (user) {
          var session = SessionService.createSession(user);
          return reply({
            success: true,
            session: session
          });
        } else {
          return reply({
            success: false,
            error: "invalid username/password"
          });
        }
      } else {
        return ErrorHandler.missingParameter(reply);
      }
    } else {
      return ErrorHandler.missingParameter(reply);
    }
  },
  logout: function (request, reply) {
    SessionService.removeSession(request.headers.sessionId);
    reply({
      success: true
    });
  },
  purchase: function (request, reply) {
    log.debug("/payment/purchase ");

    if (request && request.payload) {

      var payload = request.payload;
      var securityToken = payload.securitytoken;
      //security token is used to ensure that request is legit
      //or we can do at the netowrk level to ensure request only comes from payfirma
      var userId = payload.userid;

      if (payfirmaService.isSecurityTokenValid(securityToken, userId)) {
        var creditcardInfoToken = payload.token;

        var amount = toNumber(payload.amount);

        if (amount && creditcardInfoToken && userId) {
          return processCreditCardTransaction(creditcardInfoToken, amount, userId, reply);
        } else {
          return ErrorHandler.missingParameter(reply);
        }
      } else {
        return ErrorHandler.unauthorized(reply);
      }
    } else {
      return ErrorHandler.missingParameter(reply);
    }
  },
  getBalance: function (request, reply) {
    return reply(SessionService.getBalance(request.headers.sessionid));
  },
  getAllPlayers: function (request, reply) {
    return reply(GameEngine.getAllPlayers());
  },
  getCreditCardPaymentSecurityInfo: function (request, reply) {
    return reply(payfirmaService.getSecurityInfo());
  },
  submitRoster: function (request, reply) {
    var rosterSubmissionData = request.payload;
    if (rosterSubmissionData && rosterSubmissionData.cost) {
      var cost = rosterSubmissionData.cost;
      var userId = SessionService.getUserId(request.headers.sessionid);
      if (userId && TransactionService.addToBalance(-cost, userId)) {
        var result = GameEngine.submitRoster(rosterSubmissionData);
        var winAmount = result.winAmount;
        if (result.isWinner && winAmount) {
          TransactionService.addToBalance(winAmount+cost, userId);
        }
        return reply(result);
      } else {
        return ErrorHandler.error(reply, "Insufficient Money In Account.");
      }

    } else {
      return ErrorHandler.missingParameter(reply);
    }

  }
};

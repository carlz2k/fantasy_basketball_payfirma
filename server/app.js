/*
 * stocktwits
 * user/repo
 *
 * Copyright (c) 2015
 * Licensed under the MIT license.
 */

'use strict';
var log = require('loglevel');
log.setDefaultLevel('debug');

var routes = require('./lib/restapi/routes');
var server = require('./lib/utils/server');
var AuthorizationInterceptor = require('./lib/interceptors/AuthorizationInterceptor');
log.debug("start server");
server.addRoutes(routes.routes);
server.handleAuthorization(AuthorizationInterceptor.authorize);
server.start();

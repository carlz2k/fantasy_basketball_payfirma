'use strict';
var chai = require('chai');
chai.config.includeStack = true;
chai.use(require('chai-as-promised'));

var should = chai.should();

var gameEngine = require('../server/lib/game/GameEngine');

describe('game engine test', function () {
  it('submitRoster loser', function () {
    var rosterSubmission = {
      roster: [1, 2, 3, 4, 5],
      cost: 5
    };
    var result = gameEngine.submitRoster(rosterSubmission);
    result.isWinner.should.equal(false);
    result.winAmount.should.equal(0);
  });

  it('submitRoster winner', function () {
    var rosterSubmission = {
      roster: [1, 4, 5, 8, 9],
      cost: 5
    };
    var result = gameEngine.submitRoster(rosterSubmission);
    result.isWinner.should.equal(true);
    result.winAmount.should.least(rosterSubmission.cost);
  });
});

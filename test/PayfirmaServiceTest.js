'use strict';
var chai = require('chai');
chai.config.includeStack = true;
chai.use(require('chai-as-promised'));

var should = chai.should();

var payfirmaService = require('../server/lib/payment/payfirma/PayfirmaService');

describe('payfirma api', function () {
  it('create customer', function () {
    return payfirmaService.createCustomer({
      cardNumber: '4111111111111111',
      expiryMonth: '12',
      expiryYear: '13',
      cvv: '121',
      firstName: 'carl',
      lastName: 'zhang',
      email: 'carlzhang@gmail.com'
    });
  });
  it('do successful purchace', function () {
    return payfirmaService.purchase(51.20, {
      cardNumber: '4111111111111111',
      expiryMonth: '12',
      expiryYear: '13',
      cvv: '121'
    }).should.be.fulfilled.then(function (response) {
        response.result.should.equal("approved");
      });
  });

  it('do rejected purchace', function () {
    return payfirmaService.purchase(50.20, {
      cardNumber: '4111111111111111',
      expiryMonth: '12',
      expiryYear: '13',
      cvv: '121'
    }).should.be.fulfilled.then(function (response) {
        response.result.should.equal("declined");
      });
  });
});

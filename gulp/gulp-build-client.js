'use strict';

var gulp = require('gulp');
var del = require('del');
// Load plugins
var $ = require('gulp-load-plugins')();
var browserify = require('browserify');
var watchify = require('watchify');
var plumber = require('gulp-plumber');
var source = require('vinyl-source-stream'),
  sourceFile = './client/scripts/app.js',
  destFolder = './dist/scripts',
  destFileName = 'app.js';

var less = require('gulp-less');
// Scripts
gulp.task('client-scripts-build', function () {
  var bundler = watchify(browserify({
    entries: [sourceFile],
    insertGlobals: true,
    cache: {},
    packageCache: {},
    fullPaths: true
  }));

  bundler.on('update', rebundle);

  function rebundle() {
    return bundler.bundle()
      .pipe(plumber())
      // log errors if they happen
      .on('error', $.util.log.bind($.util, 'Browserify Error'))
      .pipe(source(destFileName))
      .pipe(gulp.dest(destFolder));
  }

  return rebundle();

});

gulp.task('less', function () {
  return gulp.src('client/css/main.less')
    .pipe(less())
    .pipe(gulp.dest('dist/css'));
});
// HTML
gulp.task('html', function () {
  return gulp.src('client/*.html')
    .pipe($.useref())
    .pipe(gulp.dest('dist'))
    .pipe($.size());
});

gulp.task('templates', function () {
  return gulp.src('client/templates/**/*.html', {base: 'client/templates'})
    .pipe($.useref())
    .pipe(gulp.dest('dist/templates'))
    .pipe($.size());
});

// Images
gulp.task('images', function () {
  return gulp.src('client/images/**/*')
    .pipe(gulp.dest('dist/images'))
    .pipe($.size());
});


// Clean
gulp.task('client-clean', function (cb) {
  cb(del.sync(['dist/**/*']));
});


// Bundle
gulp.task('client-bundle', ['client-scripts-build', 'bower', 'none_managed_components'], function () {
  return gulp.src('./client/*.html')
    .pipe($.useref.assets())
    .pipe($.useref.restore())
    .pipe($.useref())
    .pipe(gulp.dest('dist'));
});

// Webserver
gulp.task('client-run', function () {
  gulp.src('./dist')
    .pipe(plumber())
    .pipe($.webserver({
      livereload: true,
      port: 9000
    }));
});

// Bower helper
gulp.task('bower', function () {
  gulp.src('client/bower_components/**/*.js', {base: 'client/bower_components'})
    .pipe(gulp.dest('dist/bower_components/'));
  gulp.src('client/bower_components/font-awesome/fonts/**/*',
    {base: 'client/bower_components/font-awesome/fonts'})
    .pipe(gulp.dest('dist/fonts/'));

});

gulp.task('none_managed_components', function () {
  gulp.src('client/non_managed_components/**/*.js', {base: 'client/non_managed_components'})
    .pipe(gulp.dest('dist/non_managed_components/'));
});

gulp.task('json', function () {
  gulp.src('client/scripts/json/**/*.json', {base: 'client/scripts'})
    .pipe(gulp.dest('dist/scripts/'));
});

// Robots.txt and favicon.ico
gulp.task('extras', function () {
  return gulp.src(['client/*.txt', 'client/*.ico'])
    .pipe(gulp.dest('dist/'))
    .pipe($.size());
});

// Watch
gulp.task('client-changes-watch', ['client-build', 'client-run'], function () {

  // Watch .json files
  gulp.watch('client/**/*.json', ['json']);

  gulp.watch('client/scripts/**/*.js', ['client-bundle']);
  // Watch .html files
  gulp.watch('client/*.html', ['html']);

  gulp.watch('client/templates/**/*.html', ['templates']);

  gulp.watch('client/css/*.less', ['less']);
  // Watch image files
  gulp.watch('client/images/**/*', ['images']);
});

// Build
gulp.task('client-build', ['html', 'templates', 'less', 'client-bundle', 'images', 'extras']);
